﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    private int level2 = 0;
    private int level3 = 0;
    private int vidas = 3;
    private int score = 0;
    [SerializeField] GameObject candado;
    [SerializeField] GameObject candado2;
    [SerializeField] AudioSource click;


    private void Start()
    {
        vidas = 3;
        score = 0;
        level2 = PlayerPrefs.GetInt("level2", 0);
        level3 = PlayerPrefs.GetInt("level3", 0);
    }

    private void Update()
    {
        if(level2 == 5)
        {
            candado.SetActive(false);
        }
        if (level3 == 5)
        {
            candado2.SetActive(false);
        }
    }

    public void pulsaStart()
    {
        click.Play();
        new WaitForSeconds(1f);
        SceneManager.LoadScene("LEVELS");
    }

    public void creditos()
    {
        click.Play();
        new WaitForSeconds(1f);
        SceneManager.LoadScene("CREDITOS");
    }

    public void volverTitulo()
    {
        click.Play();
        new WaitForSeconds(1f);
        SceneManager.LoadScene("Menu");
    }

    public void easy()
    {
        PlayerPrefs.SetInt("score", score);
        PlayerPrefs.SetInt("vidas", vidas);
        new WaitForSeconds(1f);
        PlayerPrefs.SetInt("level2", level2);
        SceneManager.LoadScene("EASY");
    }

    public void normal()
    {
        if(level2 == 5)
        {
            new WaitForSeconds(1f);
            PlayerPrefs.SetInt("level3", level3);
            SceneManager.LoadScene("NORMAL");
        }
    }

    public void hard()
    {
        if(level3 == 5)
        {
            new WaitForSeconds(1f);
            SceneManager.LoadScene("HARD");
        }
    }

    public void exit()
    {
        Application.Quit();
    }

    public void Opciones()
    {
        SceneManager.LoadScene("Options");
    }
}
