﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public  float speed = 5;
    public float maxSpeed = 4f;
    public float jumpPower = 6.5f;
    private float h;
    public int vidas;
    public int level2 = 0;
    private Vector3 posStart;
    private Vector2 PosCheck;
    private bool checkPoint = false;

    private int effects = 1;

    private bool grounded = false;
    private bool climb = false;
    private bool jump = false;
    private bool agachar = false;
    private bool doubleJump = false;
    private bool dead = false;

    private Rigidbody2D rb2d;
    [SerializeField] Animator anim;
    [SerializeField] Transform graphics;
    private ScoreManager sm;
    [SerializeField] GameObject llave;
    [SerializeField] GameObject candado;
    [SerializeField] ParticleSystem ps;
    [SerializeField] Text lives;

    [SerializeField] AudioSource coin;
    [SerializeField] AudioSource salto;
    [SerializeField] AudioSource hit;
    [SerializeField] AudioSource check;

    void Start()
    {
        vidas = PlayerPrefs.GetInt("vidas", 0);
        rb2d = GetComponent<Rigidbody2D>();
        posStart = new Vector2(-110.95f, -1.17f);
        sm = (GameObject.Find("ScoreManager")).GetComponent<ScoreManager>();
    }

    private void FixedUpdate()
    {
        if (dead)
        {
            return;
        }
        if (agachar)
        {
            return;
        }

        h = Input.GetAxis("Horizontal");

        anim.SetFloat("velocityH", Mathf.Abs(rb2d.velocity.x));
        anim.SetFloat("velocityV", Mathf.Abs(rb2d.velocity.y));
        anim.SetBool("grounded", grounded);
        anim.SetBool("rope", climb);

        if (rb2d.velocity.x < 0)
        {
            graphics.transform.localScale = new Vector3(-2.838114f, 2.838114f, 2.838114f);
        }
        else if (rb2d.velocity.x > 0)
        {
            graphics.transform.localScale = new Vector3(2.838114f, 2.838114f, 2.838114f);
        }

        if (jump)
        {
            if (effects == 1)
            {
                salto.Play();
            }
            rb2d.velocity = new Vector2(h * speed, rb2d.velocity.y + jumpPower);
            jump = false;
        }
        else
        {
            rb2d.velocity = new Vector2(h * speed, rb2d.velocity.y);
        }

        if (rb2d.velocity.x > maxSpeed)
        {
            rb2d.velocity = new Vector2(maxSpeed, rb2d.velocity.y);
        }
        else if (rb2d.velocity.x < -maxSpeed)
        {
            rb2d.velocity = new Vector2(-maxSpeed, rb2d.velocity.y);
        }
    }

    private void Update()
    {
        PlayerPrefs.SetInt("vidas", vidas);

        if (Input.GetKeyDown(KeyCode.S))
        {
            anim.SetTrigger("duck");
            agachar = true;
        }
        else if(Input.GetKeyUp(KeyCode.S))
        {
            anim.SetTrigger("idle");
            agachar = false;
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            if (grounded == true)
            {
                jump = true;
                doubleJump = true;
            }
            else if (doubleJump == true)
            {
                jump = true;
                doubleJump = false;
            }
        }
        lives.text = vidas.ToString("0");

        if(vidas <= 0)
        {
            StartCoroutine(PasarEscena2());
        }

        if (transform.position.x >= 222)
        {
            level2 = 5;
            PlayerPrefs.SetInt("level2", level2);
            StartCoroutine(PasarEscena());
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "Ground")
        {
            grounded = true;
        }
        if (other.gameObject.tag == "Plataforma")
        {
            rb2d.velocity = new Vector3(0f, 0f, 0f);
            transform.parent = other.transform;
            grounded = true;
        }
        if (other.gameObject.tag == "Killer")
        {
            vidas--;
            if(vidas > 0)
            {
                StartCoroutine(Reaparecer());
            }
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.tag == "Ground")
        {
            grounded = false;
        }
        if (other.gameObject.tag == "Plataforma")
        {
            transform.parent = null;
            grounded = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Killer")
        {
            if (effects == 1)
            {
                hit.Play();
            }
            anim.SetTrigger("dead");
            dead = true;
            speed = 0;
            rb2d.velocity = new Vector2(0, rb2d.velocity.y + 4);
            vidas--;
            if (vidas > 0)
            {
                StartCoroutine(Reaparecer());
            }
        }

        if (other.gameObject.tag == "Check")
        {
            if (effects == 1)
            {
                check.Play();
            }
            checkPoint = true;
        }

        if (other.gameObject.tag == "Key")
        {
            if (effects == 1)
            {
                check.Play();
            }
            ps.Play();
            llave.SetActive(false);
            candado.SetActive(false);
        }

        if (other.gameObject.tag == "Coin")
        {
            if (effects == 1)
            {
                coin.Play();
            }
            sm.AddScore(50);
        }

        if (other.gameObject.tag == "Rope")
        {
            climb = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Rope")
        {
            climb = false;
        }
    }

    IEnumerator Reaparecer()
    {
        yield return new WaitForSeconds(2f);
        if(checkPoint == false)
        {
            transform.position = new Vector3(-110.95f, -1.17f, 0);
        }
        else if(checkPoint == true)
        {
            transform.position = new Vector3(44.06f, 23.85f, 0);
        }
        speed = 20;
        dead = false;
        anim.SetTrigger("idle");
    }

    IEnumerator PasarEscena()
    {
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene("LEVELS");
    }

    IEnumerator PasarEscena2()
    {
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene("LOSE");
    }
}
